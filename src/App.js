/* eslint-disable react/no-unused-state */
/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import socketClient from "socket.io-client";
import axios from "axios";
import "./App.css";
import Card from "./components/TwitterCard";
import Search from "./components/Search";
import Chart from "./components/Chart";
import Select from "./components/subComponents/select";
import { getData, createObject, initialState, mapList } from "./helper/utils";

const serverURL = "http://localhost:8000";

class App extends Component {
  state = {
    display: "1",
    input1: "",
    input2: "",
    result1: [],
    result2: [],
    result1Count: 0,
    result2Count: 0,
    searchTweet1: [],
    searchTweet2: [],
    loading: false
  };

  twitterStream = null;

  interval = null;

  socketConnect = () => {
    const socket = socketClient(serverURL);
    this.twitterStream = socket;

    socket.on("connect", () => {
      socket.on("tweets", data => {
        const { input1, input2 } = this.state;
        const object = createObject(data);
        if (object.text.includes(input1)) {
          this.setState((prevState, prevProps) => {
            const arrayResult = [...prevState.result1];
            arrayResult.unshift(object);
            return {
              result1: arrayResult.slice(0, 20),
              result1Count: prevState.result1Count + 1
            };
          });
        }
        if (object.text.includes(input2)) {
          this.setState(prevState => {
            const arrayResult = [...prevState.result2];
            arrayResult.unshift(object);
            return {
              result2: arrayResult.slice(0, 20),
              result2Count: prevState.result2Count + 1
            };
          });
        }
      });
    });
    socket.on("disconnect", () => {
      socket.off("tweets");
      socket.removeAllListeners("tweets");
      console.log("Socket Disconnected");
    });
  };

  onChangeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  searchHandlerInterval = () => {
    this.interval = setInterval(this.searchHandler, 180000);
  };

  // Start streaming
  streamHandler = event => {
    event.preventDefault();
    const { input1, input2 } = this.state;
    axios.post(
      `${serverURL}/api/data/stream?keyWord1=${input1}&keyWord2=${input2}`
    );
    this.socketConnect();
  };

  // Destroy streaming and reset UI
  resetUI = () => {
    if (this.twitterStream) {
      this.twitterStream.emit("end");
    }
    const objectState = initialState();
    clearInterval(this.interval);
    this.setState({ ...objectState });
  };

  // Get data from RESTApi
  searchHandler = async () => {
    try {
      this.setState({ loading: true });
      const { input1, input2 } = this.state;
      const searchTweet1 = await getData(input1);
      const searchTweet2 = await getData(input2);
      this.setState({ searchTweet1, searchTweet2, loading: false });
    } catch (error) {
      throw error;
    }
  };

  render() {
    const {
      loading,
      input1,
      input2,
      result1,
      result2,
      result1Count,
      result2Count,
      display,
      searchTweet1,
      searchTweet2
    } = this.state;
    let content;
    if (display === "1") {
      content = mapList([searchTweet1, searchTweet2]);
    } else {
      content = mapList([result1, result2]);
    }
    return (
      <React.Fragment>
        <h1 className="has-text-centered is-size-4 has-text-weight-bold">
          Compare the popularity with 2 keywords
        </h1>
        <hr />
        <div className="columns is-mobile is-multiline is-centered">
          <div className="column is-6-desktop is-10-mobile is-7-tablet ">
            <div className=" columns left-column">
              <div className="column ">
                <div className="box-container box-search">
                  <div className="has-text-centered is-size-5 has-text-white has-background-link title-box">
                    Search
                  </div>
                  <hr />
                  <Search
                    loading={loading ? "is-loading" : ""}
                    disabled={Boolean(input1 && input2)}
                    trackHandler={event => {
                      this.searchHandlerInterval();
                      this.streamHandler(event);
                      this.searchHandler(event);
                    }}
                    inputHandler={this.onChangeHandler}
                    resetHandler={this.resetUI}
                    value={{ input1, input2 }}
                  />
                </div>
              </div>
              <div className="column">
                <div className="box-container box-chart">
                  <div className="has-text-centered is-size-5 has-text-white has-background-link title-box">
                    Chart
                  </div>
                  <Chart
                    data={[result1Count, result2Count]}
                    labels={[input1, input2]}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="column is-5-desktop is-10-mobile is-7-tablet">
            <div className="box-container box-tweet">
              <div className="has-text-centered is-size-5 has-text-white has-background-link title-box">
                Results
              </div>
              <Select inputHandler={this.onChangeHandler} />
              <div className="columns">
                <div className="column">
                  <ul>{content[0]}</ul>
                </div>
                <div className="column">
                  <ul>{content[1]}</ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
