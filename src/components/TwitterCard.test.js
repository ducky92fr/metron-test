import React from "react";
import { shallow } from "enzyme";
import { findByTestAttr, checkProp } from "../helper/testUtils";
import "../setupTests";
import Card from "./TwitterCard";

const defaultProps = {
  image: "url",
  text: "Here is the tweet",
  url: "url"
};

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Card {...setupProps} />);
};

describe("Render component", () => {
  test("Renders <Card/> component without error", () => {
    const wrapper = setup();
    const card = findByTestAttr(wrapper, "component-card");
    expect(card.length).toBe(1);
  });
});
describe("PropType Check", () => {
  test("Does not throw warning with expected props", () => {
    const expectedProps = {
      image: "url",
      text: "Here should be a text",
      url: "url"
    };
    checkProp(Card, expectedProps);
  });
  test("Throw warning with wrong props", () => {
    const propsFailed = {
      image: 1,
      text: 2,
      url: 3
    };
    expect(() => {
      checkProp(Card, propsFailed);
    }).toThrow();
  });
});
