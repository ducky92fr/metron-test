import React from "react";
import PropTypes from "prop-types";
import { Bar } from "react-chartjs-2";
import { optionBar } from "../helper/utils";

/**
 * @function Chart - Component for chart Bar
 * @param {object} props
 * @returns {JSX.Element}
 */
const Chart = props => {
  const { data, labels } = props;
  const maxvalue = Math.max(...data);
  const chartData = {
    labels,
    datasets: [
      {
        label: "Number tweets",
        borderWidth: 1,
        data,
        backgroundColor: ["rgba(255, 99, 132, 0.6)", "rgba(54, 162, 235, 0.6)"]
      }
    ]
  };

  return (
    <div className="chart" data-test="component-chart">
      <Bar data={chartData} options={optionBar(maxvalue)} />
    </div>
  );
};

Chart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
  labels: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired
};

export default Chart;
