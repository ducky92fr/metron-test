import React from "react";
import PropTypes from "prop-types";
import Input from "./subComponents/input";
import Button from "./subComponents/button";

/**
 *@function Search - Component Search
 *@param {object} props -React props
 *@returns {JSX.Element} - Return JXS component
 */
const Search = props => {
  const {
    trackHandler,
    resetHandler,
    inputHandler,
    value,
    disabled,
    loading
  } = props;
  return (
    <form onSubmit={trackHandler} data-test="component-search">
      <div className="columns">
        <div className="column">
          <Input
            inputHandler={inputHandler}
            value={value.input1}
            name="input1"
          />
        </div>
        <div className="column">
          <Input
            inputHandler={inputHandler}
            value={value.input2}
            name="input2"
          />
        </div>
      </div>
      <div className="flex-row">
        <Button
          content="TRACK"
          type="submit"
          disabled={disabled}
          click={trackHandler}
          loading={loading}
        />
        <Button
          content="RESET"
          type="button"
          click={resetHandler}
          loading={loading}
          disabled
        />
      </div>
    </form>
  );
};
Search.propTypes = {
  loading: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  trackHandler: PropTypes.func.isRequired,
  resetHandler: PropTypes.func.isRequired,
  inputHandler: PropTypes.func.isRequired,
  value: PropTypes.objectOf(PropTypes.string.isRequired).isRequired
};
export default Search;
