import React from "react";
import { shallow } from "enzyme";
import { findByTestAttr, checkProp } from "../../helper/testUtils";
import "../../setupTests";
import Select from "./select";

const defaultProps = {
  inputHandler: () => {}
};

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Select {...setupProps} />);
};

describe("Render component", () => {
  test("Renders select without error", () => {
    const wrapper = setup();
    const select = findByTestAttr(wrapper, "component-select");
    expect(select.length).toBe(1);
  });
  test("Should handle onChange when value is passed", () => {
    const onChangeMock = jest.fn();
    const event = {
      preventDefault() {},
      target: { value: "javascript" }
    };
    const component = shallow(
      <Select {...defaultProps} inputHandler={onChangeMock} />
    );
    component.find("select").simulate("change", event);
    expect(onChangeMock).toBeCalledWith(event);
  });
  test("Select changed triggers onChange", () => {
    const onChangeMock = jest.fn();
    const component = shallow(
      <Select {...defaultProps} inputHandler={onChangeMock} />
    );
    component.find("select").simulate("change");
    expect(onChangeMock).toBeCalled();
  });
});

describe("PropType check", () => {
  test("does not throw warning with expected props", () => {
    const expectedProps = {
      inputHandler: () => {}
    };
    checkProp(Select, expectedProps);
  });

  test("throw warning with wrong props", () => {
    const propsFailed = { inputHandler: 2 };
    expect(() => {
      checkProp(Select, propsFailed);
    }).toThrow();
  });
});
