/* eslint-disable react/button-has-type */
import React from "react";
import PropTypes from "prop-types";
/**
 *@function Button - component button
 *@param {object} props -React props
 *@returns {JSX.Element} - Rendered component
 */
const Button = props => {
  const { content, click, type, disabled, loading } = props;
  return (
    <button
      className={`button is-link is-outlined is-small ${loading}`}
      type={type}
      onClick={click}
      data-test="component-button"
      disabled={!disabled}
    >
      {content}
    </button>
  );
};

Button.propTypes = {
  loading: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  content: PropTypes.string.isRequired,
  click: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired
};
export default Button;
