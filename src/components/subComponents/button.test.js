import React from "react";
import { shallow } from "enzyme";
import { findByTestAttr, checkProp } from "../../helper/testUtils";
import "../../setupTests";
import Button from "./button";

const defaultProps = {
  disabled: true,
  content: "Track",
  click: () => {},
  type: "submit",
  loading: "is-loading"
};

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Button {...setupProps} />);
};

describe("Render component", () => {
  test("Renders button without error", () => {
    const wrapper = setup();
    const button = findByTestAttr(wrapper, "component-button");
    expect(button.length).toBe(1);
  });
  test("Button disabled if 'disabled' passed by props is false", () => {
    const wrapper = setup({ disabled: false });
    const button = findByTestAttr(wrapper, "component-button");
    expect(button.props().disabled).toEqual(true);
  });

  test("Button should handle onclick", () => {
    const onClickMock = jest.fn();
    const component = shallow(<Button {...defaultProps} click={onClickMock} />);
    component.find("button").simulate("click");
    expect(onClickMock).toHaveBeenCalled();
  });
});

describe("PropType check", () => {
  test("Does not throw warning with expected props", () => {
    const expectedProps = {
      disabled: true,
      content: "Ok",
      click: () => {},
      type: "submit",
      loading: "is-loading"
    };
    checkProp(Button, expectedProps);
  });

  test("Throw warning with wrong props", () => {
    const propsFailed = {
      disabled: true,
      content: "Ok",
      click: 2,
      type: "submit",
      loading: "is-loading"
    };
    expect(() => {
      checkProp(Button, propsFailed);
    }).toThrow();
  });
});
