import React from "react";
import PropTypes from "prop-types";
/**
 *@function Input - component input
 *@param {object} props -React props
 *@returns {JSX.Element} - Rendered component
 */
const Input = props => {
  const { inputHandler, name, value } = props;
  return (
    <div className="field" data-test="component-input">
      <div className="control">
        <input
          value={value}
          name={name}
          className="input is-info"
          type="text"
          placeholder="Enter a word to track"
          onChange={inputHandler}
        />
      </div>
    </div>
  );
};

Input.propTypes = {
  inputHandler: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};
export default Input;
