import React from "react";
import PropTypes from "prop-types";
/**
 *@function Input - component input
 *@param {object} props -React props
 *@returns {JSX.Element} - Rendered component
 */
class Select extends React.PureComponent {
  render() {
    const { inputHandler } = this.props;
    return (
      <div className="has-text-centered" data-test="component-select">
        <div className="select is-small">
          <select className="is-focused" name="display" onChange={inputHandler}>
            <option value="1">Last 20 tweets</option>
            <option value="0">Real-time</option>
          </select>
        </div>
      </div>
    );
  }
}
Select.propTypes = {
  inputHandler: PropTypes.func.isRequired
};
export default Select;
