/* eslint-disable no-undef */
import React from "react";
import { shallow } from "enzyme";
import { findByTestAttr, checkProp } from "../../helper/testUtils";
import "../../setupTests";
import Input from "./input";

const defaultProps = {
  inputHandler: () => {},
  name: "input1",
  value: "javascript"
};

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Input {...setupProps} />);
};

describe("Render component", () => {
  test("Renders input without error", () => {
    const wrapper = setup();
    const input = findByTestAttr(wrapper, "component-input");
    expect(input.length).toBe(1);
  });

  test("Should handle onChange when value 'javascript' is passed", () => {
    const onChangeMock = jest.fn();
    const event = {
      preventDefault() {},
      target: { value: "javascript" }
    };
    const component = shallow(
      <Input {...defaultProps} inputHandler={onChangeMock} />
    );
    component.find("input").simulate("change", event);
    expect(onChangeMock).toBeCalledWith(event);
  });
  test("Input changed triggers onChange", () => {
    const onChangeMock = jest.fn();
    const component = shallow(
      <Input {...defaultProps} inputHandler={onChangeMock} />
    );
    component.find("input").simulate("change");
    expect(onChangeMock).toBeCalled();
  });
});

describe("PropType check", () => {
  test("Does not throw warning with expected props", () => {
    const expectedProps = {
      name: "input2",
      inputHandler: () => {},
      value: "0"
    };
    checkProp(Input, expectedProps);
  });

  test("Throw warning with wrong props", () => {
    const propsFailed = { name: 2, inputHandler: 2, value: 0 };
    expect(() => {
      checkProp(Input, propsFailed);
    }).toThrow();
  });
});
