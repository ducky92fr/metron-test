import React from "react";
import { shallow } from "enzyme";
import { findByTestAttr, checkProp } from "../helper/testUtils";
import "../setupTests";
import Chart from "./Chart";

const defaultProps = {
  labels: ["javascript", "python"],
  data: [5, 3]
};

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Chart {...setupProps} />);
};

describe("Render component", () => {
  test("Renders <Chart/> component without error", () => {
    const wrapper = setup();
    const chart = findByTestAttr(wrapper, "component-chart");
    expect(chart.length).toBe(1);
  });
});

describe("PropType check", () => {
  test("Does not throw warning with expected props", () => {
    const expectedProps = {
      data: [1, 2],
      labels: ["China", "France"]
    };
    checkProp(Chart, expectedProps);
  });

  test("Throw warning with wrong props", () => {
    const propsFailed = { data: [1, 2], labels: ["China", 2] };
    expect(() => {
      checkProp(Chart, propsFailed);
    }).toThrow();
  });

  test("Props with empty property empty will pass. In this case use not toThrow warning", () => {
    const emptyProps = { data: [], labels: [] };
    expect(() => {
      checkProp(Chart, emptyProps);
    }).not.toThrow();
  });
});
