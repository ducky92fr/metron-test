import React from "react";
import { shallow } from "enzyme";
import { findByTestAttr, checkProp } from "../helper/testUtils";
import "../setupTests";
import Search from "./Search";

const defaultProps = {
  trackHandler: () => {},
  resetHandler: () => {},
  inputHandler: () => {},
  value: { input1: "javascript", input2: "python" },
  disabled: true,
  loading: "is-loading"
};

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @returns {ShallowWrapper}
 */
const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Search {...setupProps} />);
};

describe("Render component", () => {
  test("Renders <Search/> component without error", () => {
    const wrapper = setup();
    const search = findByTestAttr(wrapper, "component-search");
    expect(search.length).toBe(1);
  });

  test("Render 2 <Button/> component without error", () => {
    const wrapper = setup();
    const buttons = wrapper.find("Button");
    expect(buttons.length).toBe(2);
  });

  test("Render 2 <Input/> component without error", () => {
    const wrapper = setup();
    const buttons = wrapper.find("Input");
    expect(buttons.length).toBe(2);
  });
});

test("Does not throw warning with expected props", () => {
  const expectedProps = {
    value: { input1: "javascript", input2: "python" },
    trackHandler: () => {},
    resetHandler: () => {},
    inputHandler: () => {},
    disabled: true,
    loading: "is-loading"
  };
  checkProp(Search, expectedProps);
});

test("Throw warning with wrong props", () => {
  const propsFailed = {
    trackHandler: 1,
    resetHandler: 2,
    inputHandler: 3,
    value: 3,
    disabled: true,
    loading: "is-loading"
  };
  expect(() => {
    checkProp(Search, propsFailed);
  }).toThrow();
});

test("Value object empty will pass. In this case use not toThrow warning", () => {
  const propsFailed = {
    value: {},
    trackHandler: () => {},
    resetHandler: () => {},
    inputHandler: () => {},
    disabled: true,
    loading: "is-loading"
  };
  expect(() => {
    checkProp(Search, propsFailed);
  }).not.toThrow();
});
