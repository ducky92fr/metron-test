import React from "react";
import PropTypes from "prop-types";

/**
 *@function CardTwitter - Component Search
 *@param {object} props -React props
 *@returns {JSX.Element} - Rendered component
 */
class Card extends React.PureComponent {
  render() {
    const { image, text, url } = this.props;
    return (
      <li className="slideDown tweet-card" data-test="component-card">
        <img alt="user-avatar" src={image} />
        <span className="text-card">{text}</span>
        <a href={url} target="_blank" rel="noreferrer noopener">
          More
        </a>
      </li>
    );
  }
}

Card.propTypes = {
  image: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
};
export default Card;
