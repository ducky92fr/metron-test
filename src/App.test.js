import React from "react";
import { shallow } from "enzyme";
import mockAxios from "axios";
import { response } from "./helper/testUtils";
import { initialState } from "./helper/utils";
import "./setupTests";
import App from "./App";

/**
 * Function to find selector
 * @function checkRender
 * @param input - Selector
 * @returns {ShallowWrapper}
 */
const checkRender = input => {
  const wrapper = shallow(<App />);
  return wrapper.find(input);
};
/**
 * Factory function to create a ShallowWrapper for the App component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state - Initial state for setup.
 * @returns {ShallowWrapper}
 */
const setup = (state = null) => {
  const wrapper = shallow(<App />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const fakeCallAxios = {
  /**
   * @function resolve - Helper function to for mockAxios resolved
   * @returns {Promise} - Return a promise resolved
   */
  resolve() {
    return mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve(response())
    );
  },
  /**
   * @function reject - Helper function to for mockAxios rejected
   * @returns {Promise} - Return a promise rejected
   */
  reject() {
    return mockAxios.post.mockImplementationOnce(() =>
      Promise.reject(new Error("failed"))
    );
  }
};

describe("Render component", () => {
  describe("Component did mount", () => {
    test("Renders <App/> component without error", () => {
      const app = checkRender("Fragment");
      expect(app.length).toBe(1);
    });
    test("App component rendered with initialState", () => {
      const wrapper = setup();
      const state = wrapper.state();
      expect(state).toEqual(initialState());
    });
    test("Render <Search/> component without error", () => {
      const search = checkRender("Search");
      expect(search.length).toBe(1);
    });
    test("Render <Chart/> component without error", () => {
      const chart = checkRender("Chart");
      expect(chart.length).toBe(1);
    });
    test("No rendering <Card/> component at beginning", () => {
      const card = checkRender("Card");
      expect(card.length).toBe(0);
    });
  });
  describe("Render when state changed", () => {
    test("Render <Card/> component with a number of tweets", () => {
      const state = {
        searchTweet1: [
          {
            image: "image",
            text: "text",
            id: 2,
            url: "url"
          }
        ],
        searchTweet2: [
          {
            image: "image",
            text: "text",
            id: 2,
            url: "url"
          }
        ]
      };
      const wrapper = setup();
      wrapper.setState(state);
      const card = wrapper.find("Card");
      expect(card.length).toBe(2);
    });
  });
});

describe("All event lead to change state", () => {
  afterEach(() => mockAxios.post.mockReset());
  test("onChangeHandler should work", () => {
    const wrapper = setup();
    wrapper
      .instance()
      .onChangeHandler({ target: { name: "display", value: "0" } });
    expect(wrapper.state().display).toEqual("0");
  });

  test("resetUI should work", () => {
    const wrapper = setup();
    wrapper.instance().resetUI();
    const state = wrapper.state();
    expect(state).toEqual(initialState());
    expect(wrapper.instance().interval).toBeNull();
  });

  test("searchHandler with axios should work with resolve ", async () => {
    fakeCallAxios.resolve();
    fakeCallAxios.resolve();
    const wrapper = setup();
    await wrapper.instance().searchHandler();
    expect(mockAxios.post).toHaveBeenCalledTimes(2);
    expect(wrapper.state().searchTweet1.length).toBe(1);
    expect(wrapper.state().searchTweet2.length).toBe(1);
  });
  test("searchHandler with axios should work with reject", async () => {
    fakeCallAxios.reject();
    fakeCallAxios.reject();
    const wrapper = setup();
    await expect(wrapper.instance().searchHandler()).rejects.toThrow();
  });

  test("streamHandler with axios should work", () => {
    const wrapper = setup();
    const event = {
      preventDefault() {}
    };
    wrapper.instance().streamHandler(event);
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
  });

  test("searchHandlerInterval should work", () => {
    const wrapper = setup();
    wrapper.instance().searchHandlerInterval();
    expect(wrapper.instance().interval).not.toBeNull();
  });
});
