/* eslint-disable import/prefer-default-export */
import axios from "axios";
import React from "react";
import Card from "../components/TwitterCard";

const serverURL = "http://localhost:8000";

/**
 *@function createObject - helper function to clean a tweet object
 * @param {object} data  - tweet object
 * @returns {object}
 */
export const createObject = data => {
  return {
    image: data.user.profile_image_url,
    text: data.text,
    id: data.id_str,
    url: `https://twitter.com/${data.in_reply_to_screen_name}/status/${data.id_str}`
  };
};

/**
 * @function getData - helper function to get data tweets
 * @param {string} keyWord - Word to search
 * @param {string} input - Property for setState
 * @returns {promise}
 */
export const getData = keyWord => {
  return axios
    .post(`${serverURL}/api/data/search?keyWord=${keyWord}`)
    .then(tweets => {
      return tweets.data.tweets.statuses.map(el => createObject(el));
    })
    .catch(err => {
      throw err;
    });
};

/**
 * @function initialState - helper function to create an itinial state
 * @returns {object}
 */
export const initialState = () => {
  return {
    display: "1",
    input1: "",
    input2: "",
    result1: [],
    result2: [],
    result1Count: 0,
    result2Count: 0,
    searchTweet1: [],
    searchTweet2: [],
    loading: false
  };
};

/**
 * @function mapList - helper function to generate list JSX
 * @param {array} array
 * @returns {JSX.Element}
 */
export const mapList = array => {
  return array.map(result =>
    result.map(el => (
      <Card
        image={el.image}
        text={el.text}
        key={el.id}
        date={el.date}
        url={el.url}
      />
    ))
  );
};

/**
 * @function optionBar - setup Option for bar component
 * @param {number} maxvalue
 * @returns {Object}
 */
export const optionBar = maxvalue => {
  return {
    scales: {
      yAxes: [
        {
          ticks: {
            suggestedMax: maxvalue + 1,
            beginAtZero: true
          }
        }
      ],
      xAxes: [
        {
          barPercentage: 0.3
        }
      ]
    }
  };
};
