/* eslint-disable react/forbid-foreign-prop-types */
import checkProps from "check-prop-types";
/**
 * Return node(s) with the given data-test attribute
 * @param {ShallowWrapper} wrapper -Enzyme shallow wrapper.
 * @param {string} val  -Value of data-test attribute for search.
 * @returns {ShallowWrapper}
 */
export const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

/**
 * @function checkProp - helper function to check propType for testing
 * @param {wrapper} component
 * @param {propsExpected} conformingProps
 * @returns {Boolean}
 */
export const checkProp = (component, conformingProps) => {
  const propError = checkProps(
    component.propTypes,
    conformingProps,
    "prop",
    component.name
  );
  expect(propError).toBeUndefined();
};

/**
 * @function response - Factory function to create a response object from server for testing
 */
export const response = () => {
  return {
    data: {
      tweets: {
        statuses: [
          {
            user: {
              profile_image_url:
                "http://pbs.twimg.com/profile_images/947960919706959875/W9jrCWFF_normal.jpg"
            },
            text: "This is a fake tweet",
            id_str: "1144206029498068992",
            in_reply_to_screen_name: null
          }
        ]
      }
    }
  };
};
