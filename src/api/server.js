/* eslint-disable no-unused-expressions */
const app = require("express")();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
const bodyParser = require("body-parser");
const Twitter = require("twitter");

const keyAPI = require("./twitterApi/key");

const client = new Twitter(keyAPI);
const port = 8000;

// Middleware
app.use(bodyParser.json()); // for application/json

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

let socketConnection;
let twitterStream;

// Using RESTApi
app.post("/api/data/search", (req, res) => {
  const { keyWord } = req.query;
  client
    .get("search/tweets.json", {
      q: keyWord,
      count: 20
    })
    .then(tweets => {
      res.json({ tweets });
    })
    .catch(err => {
      console.log(err);
    });
});

// Using streaming API
app.post("/api/data/stream", req => {
  twitterStream ? twitterStream.destroy() : twitterStream;
  const { keyWord1, keyWord2 } = req.query;

  client.stream(
    "statuses/filter",
    { track: `${keyWord1},${keyWord2}` },
    stream => {
      stream.on("data", tweet => {
        if (!tweet.text.includes("RT")) {
          socketConnection.emit("tweets", tweet);
        }
      });

      stream.on("error", error => {
        console.log(error);
      });
      twitterStream = stream;
    }
  );
});

// Etablish socket connection
io.on("connection", socket => {
  socketConnection = socket;
  console.log("User connected");
  socket.on("disconnect", () => {
    twitterStream ? twitterStream.destroy() : twitterStream;
    console.log("User disconnected");
  });
  socket.on("end", () => {
    socket.disconnect();
  });
});

server.listen(port, () => {
  console.log(`Server is connect at ${port}`);
});
